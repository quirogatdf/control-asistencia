const { config } = require('dotenv')
const { SERVER } = require('./src/config/index');
const { makeServer } = require('./server');
const { connection } = require('./src/database/db');

async function main() {
  config ()
  connection;
  
  /* Inicializar la DB */
  //require('./src/database/db');
  /* Incializar Express */
  const app = makeServer();
  app.listen(SERVER.PORT, () => {
    const m = new Date();
    const ahora = m.getUTCFullYear() +"/"+ (m.getUTCMonth()+1) +"/"+ m.getUTCDate() + " " + m.getUTCHours() + ":" + m.getUTCMinutes() + ":" + m.getUTCSeconds();
    console.log(`La aplicación se encuentra corriendo en el puerto ${SERVER.PORT} desde el ${ahora}`);
  })
}

main ();