const express = require('express')
const cors = require('cors');
const helmet = require('helmet');


function makeServer() {
  /* Configuramos nuestro servidor Express*/
  const app = express();
  app.use(cors());
  app.use(helmet());
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  const DocenteRoute = require('./src/routes/docente.route');
  const AttedanceRoute = require('./src/routes/attedance.route');
  app.use('/api',DocenteRoute);
  app.use('/api',AttedanceRoute);

  return app;

}

module.exports = {
  makeServer,
};