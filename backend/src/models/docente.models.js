//Docente object create
const DocenteModel = function (docente) {
  this.dni = docente.dni;
  this.nombre = docente.nombre;
  this.apellido = docente.apellido;
  this.fecha_nacimiento = docente.fecha_nacimiento;
  this.created_at = new Date();
  this.updated_at = new Date();
}

module.exports = { DocenteModel };