const express = require('express');
const router = express.Router();

const AttedanceController = require('../controllers/attedance.controllers');


router
      .post('/registrar-asistencia', AttedanceController.iniciar)
      .put('/finalizar-asistencia', AttedanceController.finalizarAsistencia)
      
module.exports = router;