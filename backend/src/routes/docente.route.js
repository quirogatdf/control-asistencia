const express = require('express');
const router = express.Router();

const DocenteController = require('../controllers/docente.controllers');


router
      .post('/docente/add', DocenteController.create)
      .get('/docente', DocenteController.findAll)
      .get('/docente/:id', DocenteController.findById)
      .get('/register', DocenteController.findByDni)
      .put('/docente/:id/edit', DocenteController.update)
      .delete('/docente/:id/delete', DocenteController.delete)
module.exports = router;