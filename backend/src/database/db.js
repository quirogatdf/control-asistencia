const mysql = require('mysql');
const { DB } = require('../config/index');
/* Parametros de conexion a la BD */
const connectionData = {
  host: DB.HOST,
  user: DB.USER,
  password: DB.PASSWORD,
  database: DB.DATABASE,
  port: DB.PORT,
  dialect: DB.DIALECT,
}

/* Crear conexion a la BD */
const connection = mysql.createConnection(connectionData)

//Prueba de conexion
connection.connect(function (err) {
  if (err) {
    console.error(`error connnecting: ${err.stack}`)
    return;
  }
  console.log(`connected as id ${connection.threadId}`);
});


module.exports = { connection };