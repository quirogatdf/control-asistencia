'use strict'
const { AttedanceModel } = require('../models/attedance.models');
const AttedanceService = require('../services/attedance.service');

exports.iniciar = async (req, res) => {
  const asistencia = [{
    docenteId: req.body.dni,
    fecha_inicio: new Date(),
    estado: true,

  }]
  await AttedanceService.findActive(req.body.dni, (err, data) => {
    if (!(data.length <=0)) {
      res.status(200).send({ status: true, message: 'Ud. ya tiene una asistencia activa' })
    } else {
      AttedanceService.createAsistencia(asistencia, (err, data) => {
        if (err)
        res.send(err)
      })
      res.status(200).send({ status: false, message: 'Asistencia registrada correctamente' })
    }
  })
  
}

exports.finalizarAsistencia = async (req, res) => {
  const dni = req.body.dni
  await AttedanceService.findActive(req.body.dni, (err, data) => {
    if(!(data.length <=0)) {
      AttedanceService.finalizarAsistencia(dni, (err, d) => {
        if(err)
        res.send(err)
        console.log(d);
      })

      res.status(200).send({ registered: true, message: 'Ud. finalizó correctamente su jornada.' })
    } else {
      res.status(200).send({ registered: false, message: 'No se encontró una asistencia activa.' })

    }
  })

}

