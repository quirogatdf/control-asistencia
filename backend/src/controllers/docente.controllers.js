'use strict'
const { DocenteModel } = require('../models/docente.models');
const DocenteService = require('../services/docente.service');

exports.create = (req, res) => {
  const new_docente = new DocenteModel(req.body)
  if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
    res.status(400).send({ error: true, message: 'Please provide all required field' });
  } else {
    DocenteService.create(new_docente, (err, docente) => {
      if (err)
        res.send(err);
      res.json({ error: false, message: 'Docente added successfuly', data: docente });
    })
  }
}

exports.findAll = async (req, res) => {
  DocenteService.findAll((err, docente) => {
    if (err) {
      res.status(400).send(err);
    } else {
      res.send(docente);
    }
  })
}

exports.findById = async (req, res) => {
  const id = req.params.id;
  DocenteService.findById(id, (err, docente) => {
    if (err)
      res.status(400).send(err);
    res.status(200).json(docente);
  })

};
exports.findByDni = async (req, res) => {
  let dni = req.query.dni;
  DocenteService.findByDni(dni, (err, docente) => {
    docente.length <= 0? res.status(200).send({state: false, data: null}) : res.status(200).send({state: true, data: docente})
  })

};

exports.update = async (req, res) => {
  const { id } = req.params
  if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
    res.status(400).send({ error: true, message: 'Please provide all required field' });
  } else {
    DocenteService.update(id, new DocenteModel(req.body), (err, docente) => {
      console.log(id);
      if (err) {
        res.send(err);
      } else {
        console.log(req.body)
        res.json({ error: false, message: 'Docente successfully updated' });
      }
    });
  }
}

exports.delete = async (req, res) => {
  const {id} = req.params
  DocenteService.delete(id, (err, docente) => {
    err ? res.status(400).send(err) : res.status(200).json({error: false, message:'Docente successfully deleted'});
  })
}
