const { AttedanceModel: Attedance } = require('../models/attedance.models');
const { connection } = require('../database/db');

class AttedanceService {
  /* Registrar asistencia */
  static async createAsistencia(asistencia, result) {
    await connection.query('INSERT INTO attedance SET ?', asistencia, (err, res) => {
      if (err) throw err;
    })
  }
  static async finalizarAsistencia(docenteId, result) {
    const fecha_fin = new Date();
    const sql = 'UPDATE attedance SET fecha_fin = ?, estado = ? WHERE attedance.docenteId = ? and estado = 1';
    await connection.query(sql,[fecha_fin,0,docenteId], (err, res) => {
      err ? result (err, null) : result(null, console.log(JSON.parse(JSON.stringify(res))))
    })
  }

  static async findActive(dni,result) {
    const sql = 'SELECT * FROM attedance WHERE estado = ? and docenteId = ?'
    await connection.query(sql, [1, dni], (err, res) => {
      err ? result(err, null) : result(null, JSON.parse(JSON.stringify(res)));
    })
  }

}


module.exports = AttedanceService;