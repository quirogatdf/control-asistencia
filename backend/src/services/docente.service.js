const { DocenteModel: Docente } = require('../models/docente.models');
const { connection } = require('../database/db');

class DocenteService {
  /* Crear un nuevo docente */
  static async create(newDocente, result) {
    await connection.query('INSERT INTO docentes SET ?', newDocente, (err, res) => {
      if (err) {
        result(err, null);
      } else {
        result(null, res.insertId);
      }
    })
  }
  /* Buscar todos los docentes */
  static async findAll(result) {
    await connection.query('SELECT * FROM docentes', (err, res) => {
      if (err) {
        result(null, err);
      } else {
        result(null, (res));
      }
    })
  }
  /* Buscar docente por Id. */
  static async findById(id, result) {
    await connection.query('SELECT * FROM docentes WHERE id = ?', [id], (err, res) => {
      err ? result(err, null) : result(null, res);
    });
  }
  static async findByDni(dni, result){
    await connection.query('SELECT * FROM docentes WHERE dni = ?', [dni], (err, res) => {
     err ? result(err, null) : result(null, res);
    

    })
  }
  /*Modificar registro del docente */
  static async update(id, docente, result) {
    await connection.query('UPDATE docentes SET dni=?, nombre=?, apellido=?, fecha_nacimiento=? WHERE id=?', [docente.dni, docente.nombre, docente.apellido, docente.fecha_nacimiento, id], (err, res) => {
      err ? result(err, null) : result(null, res);
    });
  }

  static async delete(id, result) {
    await connection.query('DELETE FROM docentes WHERE id = ?', [id], (err, res) => {
      err ? result(err, null) : result(null, res);
    });
  }
}

module.exports = DocenteService;
