require ('dotenv').config();

module.exports = {
  SERVER: {
    PORT: process.env.SERVER_PORT || 3000,
  },
  DB: {
    USER: process.env.MYSQL_USER,
    PASSWORD: process.env.MYSQL_PASSWORD,
    DATABASE: process.env.MYSQL_DATABASE,
    HOST: process.env.MYSQL_HOST,
    PORT: process.env.MYSQL_PORT || 5432,
    DIALECT: process.env.MYSQL_DIALECT
  }
}