export class DocenteModel {
  id?: any;
	dni? : number;
	nombre? : string;
	apellido? : string;
	fecha_nacimiento? : Date;
	created_at? : Date;
  updated_at?: Date;
}