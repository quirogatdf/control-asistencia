
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DocenteModel } from '../../models/docente.models';
import { DocenteService } from '../../services/docente.service';


@Component({
  selector: 'app-docente',
  templateUrl: './docente.component.html',
  styleUrls: ['./docente.component.css'],
  //providers: [DocenteService]
})
export class DocenteComponent implements OnInit {

  rows?: DocenteModel[];


 @Output() deleteRequest = new EventEmitter<DocenteModel>();


  constructor(
    private docenteService: DocenteService
  ){}

  ngOnInit(): void {
    this.getDocentes();
  }

  getDocentes(): void {
    this.docenteService.getAll().subscribe(
      data => {
        this.rows = data;
        console.log(data);
      },
      err => {
        console.log(err);
      }
    )
  }
  deleteDocente(id: any): void {
    if (window.confirm('Are you sure?')){

      this.docenteService.delete(id)
        .subscribe(
          data  => {
            this.getDocentes();
          },
          error => {
            console.log(error);
          });
    }
    }
}
