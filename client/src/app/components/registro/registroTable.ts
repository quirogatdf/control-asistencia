export interface TableColumn {
  id?: number;
  dni: number;
  entry: string; // column name
  exit: string; // name of key of the actual data in this column
  status?: boolean;
}