import { Component, OnInit } from '@angular/core';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
import { DocenteModel } from 'src/app/models/docente.models';
import { formatDate } from '@angular/common';

import Swal from 'sweetalert2';
import { AttedanceService } from '../../services/attedance.service';
import { DocenteService } from '../../services/docente.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  docente?: DocenteModel;
  user: any

  constructor(
    private docenteService: DocenteService,
    private attedanceService: AttedanceService
  ) { }

  ngOnInit(): void {
  }
  finalizarAsistencia() {
    Swal.fire({
      title: 'Bienvenido',
      input: 'text',
      inputLabel: 'Ingrese su DNI',
      inputPlaceholder: 'Enter your dni'
    }).then((dni) => {
      this.docenteService.getByDni(dni.value).subscribe(data => {
        this.user = data
        if (this.user.state) {
          Swal.fire({
            text: `${this.user.data[0].apellido} ${this.user.data[0].nombre}, confirme su asistencia`,
            title: 'Si usted es:',
            showDenyButton: true,
            confirmButtonColor: '#09b154',
            confirmButtonText: 'Confirmar',
            denyButtonText: 'Cancelar',
          }).then((res) => {
            if (res.isConfirmed) {
              this.attedanceService.finalizarAsistencia(this.user.data[0].dni).subscribe(dat => {
                console.log(dat);
                if (dat.registered) {
                  const date = formatDate(new Date(), 'dd/MM/YYYY, HH:mm', 'en');
                  Swal.fire('Saved!', `${dat.message}`, 'success')
                } else {
                  Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: dat.message,

                  })
                }
              })
            }
          })
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Verifique que ingreso su DNI correctamente',
          })
        }
      })
    })
  }

  onClick() {

    Swal.fire({
      title: 'Bienvenido',
      input: 'text',
      inputLabel: 'Ingrese su DNI',
      inputPlaceholder: 'Enter your dni'
    }).then((dni) => {
      this.docenteService.getByDni(dni.value).subscribe(data => {
        this.user = data
        if (this.user.state) {
          Swal.fire({
            text: `${this.user.data[0].apellido} ${this.user.data[0].nombre}, confirme su asistencia`,
            title: 'Si usted es:',
            showDenyButton: true,
            confirmButtonColor: '#09b154',
            confirmButtonText: 'Confirmar',
            denyButtonText: 'Cancelar',
          }).then((res) => {
            if (res.isConfirmed) {
              this.attedanceService.iniciarAsistencia(this.user.data[0].dni).subscribe(dat => {
                console.log(dat);
                if (!dat.status) {
                  const date = formatDate(new Date(), 'dd/MM/YYYY, HH:mm', 'en');
                  Swal.fire('Saved!', `Asistencia registrada el día ${date}`, 'success')
                } else {
                  Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: dat.message,

                  })
                }
              })
            }
          })
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Verifique que ingreso su DNI correctamente',
          })
        }
      })
    })
  }

}
