import { Injectable } from '@angular/core';
import { HttpClient, HttpParams} from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { AttedanceModel } from '../models/attedance.models';

const baseUrl = 'http://localhost:4000/api'

@Injectable({
  providedIn: 'root'
})
export class AttedanceService {
  constructor(private http: HttpClient) { }

  iniciarAsistencia(dni: number): Observable <any> {
    return this.http.post<any>(`${baseUrl}/registrar-asistencia`,{'dni':dni});
  }
  finalizarAsistencia(dni:number): Observable <any> {
    return this.http.put<any>(`${baseUrl}/finalizar-Asistencia`, {'dni':dni});
  }
}