import { Injectable } from '@angular/core';
import { HttpClient, HttpParams} from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { DocenteModel } from '../models/docente.models';

const baseUrl = 'http://localhost:4000/api/docente'

@Injectable({
  providedIn: 'root'
})
export class DocenteService {
  public docente :any =  ""
  constructor(private http: HttpClient) { }

  getAll(): Observable<DocenteModel[]> {
    return this.http.get<DocenteModel[]>(baseUrl);
  }
  getById(id: any): Observable<DocenteModel> {
    return this.http.get(`${baseUrl}/${id}`);
  }
  public getByDni(dni: number): Observable<any>{
    let queryParams = new HttpParams();
    queryParams = queryParams.append('dni',dni)
    return this.http.get<DocenteModel>(`http://localhost:4000/api/register`,{params: queryParams})
  }
  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}/delete`);
  }
}
